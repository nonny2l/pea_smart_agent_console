﻿using RestSharp;
using RRApiFramework.Utility;
using RRPlatFormConsole.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace RRPlatFormConsole.SendSMS
{
    public class SendSMSRest
    {
        private const string _reservedCharacters = "!*'();:@&=+$,/?%#[]";

        public static IRestResponse SendSMSByRest(string mobileNumber, string message)
        {
            string bulkEndpoint = WebConfigurationManager.AppSettings["BulkEndpoint"].ToString();
            string bulkAccount = WebConfigurationManager.AppSettings["BulkAccount"].ToString();
            string bulkPassword = WebConfigurationManager.AppSettings["BulkPassword"].ToString();

            var client = new RestClient(bulkEndpoint);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("ACCOUNT", bulkAccount);
            request.AddParameter("PASSWORD", bulkPassword);
            request.AddParameter("MOBILE", mobileNumber);
            request.AddParameter("MESSAGE", UrlEncode(message));
            IRestResponse response = client.Execute(request);

            return response;
        }



        public static string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (_reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int)@char);
            }
            return sb.ToString();
        }
        public static ContentSmsResponse BindSmsResponse(string contentString)
        {
            ContentSmsResponse contentSmsResponse = new ContentSmsResponse();
            if (!contentString.IsNullOrEmptyWhiteSpace())
            {
                contentSmsResponse.STATUS = MessageManagement.FindStringFromIndex(contentString, "STATUS=", 3);
                contentSmsResponse.MESSAGE_ID = MessageManagement.FindStringFromIndex(contentString, "MESSAGE_ID=", 9);
                contentSmsResponse.TASK_ID = MessageManagement.FindStringFromIndex(contentString, "TASK_ID=", 7);
                contentSmsResponse.END = MessageManagement.FindStringFromIndex(contentString, "END=", 2);
            }

            return contentSmsResponse;

        }
    }
    public class ContentSmsResponse
    {
        public string STATUS { get; set; }
        public string MESSAGE_ID { get; set; }
        public string TASK_ID { get; set; }
        public string END { get; set; }

    }


}
