﻿using Newtonsoft.Json;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormConsole.MailGun;
using System;
using System.Drawing;
using System.IO;
using IronBarCode;
using System.Linq;
using System.Collections.Generic;
using log4net;
using RRPlatFormConsole.SendSMS;
using System.Threading.Tasks;
using RestSharp;
using System.Web.Configuration;
using System.Text;

namespace RRPlatFormConsole.SchedulerJob
{
    public class SchedulerJobAction
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SchedulerJobAction));

        public bool AutoGenerateBarCode()
        {
            bool result = false;
            CustomerProfile customerProfile = new CustomerProfile();
         
            customerProfile.Datas = customerProfile.GetListByUnGenarateBarcode();
            foreach (var customerProfileData in customerProfile.Datas)
            {
                string codeGen = customerProfileData.URL;
                Image QRbarcode = QRCodeWriter.CreateQrCode(codeGen, 128, QRCodeWriter.QrErrorCorrectionLevel.Medium).Image;
                byte[] base64Byte = ImageToByte(QRbarcode);
                string base64String = Convert.ToBase64String(base64Byte);

                customerProfileData.QR_CODE = "data:image/jpg;base64," + base64String;
                customerProfile.Edit("", customerProfileData);
                result = true;
            }
            return result;

        }
       
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public static string GenerateMessageId(string customerId)
        {
            string result = string.Empty;
            result = customerId + DateTime.Now.ToString("yyyyMMddhhmmssfff");
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">yyyy-MM-dd</param>
        /// <returns></returns>
        public string ConvertDatetoString(DateTime date)
        {
            try
            {
                Dictionary<string, string> monthList = new Dictionary<string, string>
                                                         {
                                                             { "01", "มกราคม" },
                                                             { "02", "กุมภาพันธ์" },
                                                             { "03", "มีนาคม" },
                                                             { "04", "เมษายน" },
                                                             { "05", "พฤษภาคม" },
                                                             { "06", "มิถุนายน" },
                                                             { "07", "กรกฎาคม" },
                                                             { "08", "สิงหาคม" },
                                                             { "09", "กันยายน" },
                                                             { "10", "ตุลาคม" },
                                                             { "11", "พฤศจิกายน" },
                                                             { "12", "ธันวาคม" }
                                                         };

                    string month = date.Month.ToString("00");
                    int year = date.Year;
                    // return monthList[month] + " " + (int.Parse(year) + 543);
                    if (year < 2500)
                    {
                        return date.Day.ToString() + " " + monthList[month] + " " + (year + 543);
                    }
                    else
                    {
                        return date.Day.ToString() + " " + monthList[month] + " " + (year);
                    }
                
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        public bool AutoJobSendEmailEDM1()
        {
            bool result = false;
            CustomerProfile customerProfile = new CustomerProfile();
            ContratList contratList = new ContratList();
            SendEmailManagement sendEmailManagement = new SendEmailManagement();
            EdmH edmH = new EdmH();

            customerProfile.Datas = customerProfile.GetListByIsActiveActivity(StatusYesNo.Yes.Value(), (int)ActivityStatus.WaitEdm);

            log.Info("--- Start Customer Count : "+ customerProfile.Datas.Count + " ---");

            foreach (var customerProfileData in customerProfile.Datas)
            {

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO +" Name : " + customerProfileData.CUSTOMER_NAME + " ---");

                var CCEmailList = contratList.GetDatasByMainStatus(customerProfileData.CUSTOMER_ID.ToString(), StatusYesNo.No.Value());
                var MainEmailList = contratList.GetDatasByMainStatus(customerProfileData.CUSTOMER_ID.ToString(), StatusYesNo.Yes.Value());

                foreach (var toMainEmail in MainEmailList)
                {
                    result = false;

                    ContentRequest contentRequest = new ContentRequest();
                    contentRequest.CF_url = customerProfileData.URL;

                    string fullName = string.Empty;

                    if(toMainEmail.CONTRACT_NAME.IsNullOrEmptyWhiteSpace())
                        fullName = string.Format("ท่านผู้บริหาร {0}", customerProfileData.CUSTOMER_NAME);
                    else
                        fullName = string.Format("คุณ {0} {1}", toMainEmail.CONTRACT_NAME, customerProfileData.CUSTOMER_NAME);

                    contentRequest.CF_fullname = fullName;
                    contentRequest.CF_qr_code = customerProfileData.QR_CODE;
                    contentRequest.CF_send_date = ConvertDatetoString(DateTime.Now);

                    string conteneHtml = JsonConvert.SerializeObject(contentRequest);

                    SendEmailTransactionalTemplateRequest mailContent = new SendEmailTransactionalTemplateRequest();
                    mailContent.priority = "0";
                    mailContent.from_name = "PEA Solar Hero";
                    mailContent.from_email = "peasolarhero@pea.co.th";
                    mailContent.to_name = toMainEmail.CONTRACT_NAME;
                    mailContent.to_email = toMainEmail.EMAIL;
                    mailContent.subject = "#เลือกให้ดีขึ้น ติดตั้ง Solar Rooftop กับ PEA พร้อมมีผู้ลงทุนและวางระบบครบวงจร";
                    mailContent.template_key = "40825e7a143b3e9d9";
                    mailContent.content_html = conteneHtml;
                    mailContent.transactional_group_name = "EDM01";
                    mailContent.message_id = GenerateMessageId(customerProfileData.CUSTOMER_ID.ToString());

                    mailContent.cc = CCEmailList.Select(a => new CCProperty()
                    {
                        name = a.CONTRACT_NAME,
                        email = a.EMAIL
                    }).ToList();


                    if (CCEmailList.Count != 0)
                    {
                        var responseMail = sendEmailManagement.SendEmailTransactionalTemplateCC(mailContent);

                        if (responseMail.code == 202 && responseMail.data != null)
                        {
                            result = true;
                            foreach (var contratListData in CCEmailList)
                            {
                                edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, contratListData.ID);
                                if (edmH.Data == null)
                                {
                                    edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                    edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                    edmH.Data.CA_NO = customerProfileData.CA_NO;
                                    edmH.Data.EMD_TYPE = 0;
                                    edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                    edmH.Data.SEND_DATE = DateTime.Now;
                                    edmH.Data.LAST_EMAIL_STATUS = "W";
                                    edmH.Data.CONTRACT_ID = contratListData.ID;
                                    edmH.Data.EMAIL = contratListData.EMAIL;
                                    edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                    edmH.Create(edmH.Data);
                                }
                            }

                            edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, toMainEmail.ID);
                            if (edmH.Data == null)
                            {
                                edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                edmH.Data.CA_NO = customerProfileData.CA_NO;
                                edmH.Data.EMD_TYPE = 0;
                                edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                edmH.Data.SEND_DATE = DateTime.Now;
                                edmH.Data.LAST_EMAIL_STATUS = "W";
                                edmH.Data.CONTRACT_ID = toMainEmail.ID;
                                edmH.Data.EMAIL = toMainEmail.EMAIL;
                                edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                edmH.Create(edmH.Data);
                            }
                        }

                    }
                    else
                    {
                        var responseMail = sendEmailManagement.SendEmailTransactionalTemplateTo(mailContent);
                        if (responseMail.code == 202 && responseMail.data != null)
                        {
                            result = true;

                            edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, toMainEmail.ID);
                            if (edmH.Data == null)
                            {
                                edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                edmH.Data.CA_NO = customerProfileData.CA_NO;
                                edmH.Data.EMD_TYPE = 0;
                                edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                edmH.Data.SEND_DATE = DateTime.Now;
                                edmH.Data.LAST_EMAIL_STATUS = "W";
                                edmH.Data.CONTRACT_ID = toMainEmail.ID;
                                edmH.Data.EMAIL = toMainEmail.EMAIL;
                                edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                edmH.Create(edmH.Data);
                            }
                        }
                    }

                }

                if (result)
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.SendEdm1Complete;
                else
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.Error;

                customerProfile.Edit("", customerProfileData);

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Status : " + customerProfileData.ACTIVITY_STATUS + " ---");

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");
            }

            log.Info("--- End Customer Count : " + customerProfile.Datas.Count + " ---");

            return result;

        }

        public bool SendSmsEdm5()
        {
            bool result = false;
            CustomerProfile customerProfile = new CustomerProfile();
            ContratList contratList = new ContratList();
            TrSendSmsH trSendSmsH = new TrSendSmsH();

            customerProfile.Datas = customerProfile.GetListByIsActiveActivity(StatusYesNo.Yes.Value(), (int)ActivityStatus.WaitEdm);

            log.Info("--- Start Customer Count : " + customerProfile.Datas.Count + " ---");

            foreach (var customerProfileData in customerProfile.Datas)
            {

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");

                var MainSendSmsList = contratList.GetDatas(customerProfileData.CUSTOMER_ID.ToString());

                foreach (var MainSendSms in MainSendSmsList)
                {
                    result = false;
                    string smsMessage = string.Empty;

                    string fullName = string.Empty;

                    if (MainSendSms.CONTRACT_NAME.IsNullOrEmptyWhiteSpace())
                        fullName = string.Format("ท่านผู้บริหาร {0}", customerProfileData.CUSTOMER_NAME);
                    else
                        fullName = string.Format("คุณ {0} {1}", MainSendSms.CONTRACT_NAME, customerProfileData.CUSTOMER_NAME);


                   
                    var responseSms = SendSMSRest.SendSMSByRest(MainSendSms.TEL, smsMessage);
                    var responseSmsContent = SendSMSRest.BindSmsResponse(responseSms.Content);
                    if (responseSmsContent.STATUS == "0")
                    {
                        trSendSmsH.Save(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, MainSendSms.ID, 4,
                            smsMessage, responseSmsContent.STATUS, responseSmsContent.MESSAGE_ID, responseSmsContent.TASK_ID);
                    }
                    else
                    {
                        trSendSmsH.Save(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, MainSendSms.ID, 4,
                            smsMessage, responseSmsContent.STATUS, responseSmsContent.MESSAGE_ID, responseSmsContent.TASK_ID);
                    }
                }

                if (result)
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.SendEdm1Complete;
                else
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.Error;

                customerProfile.Edit("", customerProfileData);

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Status : " + customerProfileData.ACTIVITY_STATUS + " ---");

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");
            }

            log.Info("--- End Customer Count : " + customerProfile.Datas.Count + " ---");

            return result;

        }
       
        public bool AutoGetEmailStatus()
        {
            bool result = false;



            return result;

        }
        public bool AutoJobSendEmailEDM2()
        {
            bool result = false;
            CustomerProfile customerProfile = new CustomerProfile();
            ContratList contratList = new ContratList();
            SendEmailManagement sendEmailManagement = new SendEmailManagement();
            EdmH edmH = new EdmH();

            customerProfile.Datas = customerProfile.GetListByIsActiveActivity(StatusYesNo.Yes.Value(), (int)ActivityStatus.WaitEdm);

            log.Info("--- Start Customer Count : " + customerProfile.Datas.Count + " ---");

            foreach (var customerProfileData in customerProfile.Datas)
            {
                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");

                var CCEmailList = contratList.GetDatasByMainStatus(customerProfileData.CUSTOMER_ID.ToString(), StatusYesNo.No.Value());
                var MainEmailList = contratList.GetDatasByMainStatus(customerProfileData.CUSTOMER_ID.ToString(), StatusYesNo.Yes.Value());

                foreach (var toMainEmail in MainEmailList)
                {
                    result = false;

                  

                    string fullName = string.Empty;

                    if (toMainEmail.CONTRACT_NAME.IsNullOrEmptyWhiteSpace())
                        fullName = string.Format("ท่านผู้บริหาร {0}", customerProfileData.CUSTOMER_NAME);
                    else
                        fullName = string.Format("คุณ {0} {1}", toMainEmail.CONTRACT_NAME, customerProfileData.CUSTOMER_NAME);


                    ContentRequest contentRequest = new ContentRequest();
                    contentRequest.CF_url = customerProfileData.URL;
                    contentRequest.CF_fullname = fullName;
                    contentRequest.CF_qr_code = customerProfileData.QR_CODE;
                    contentRequest.CF_send_date = ConvertDatetoString(DateTime.Now);

                    string conteneHtml = JsonConvert.SerializeObject(contentRequest);

                    SendEmailTransactionalTemplateRequest mailContent = new SendEmailTransactionalTemplateRequest();
                    mailContent.priority = "0";
                    mailContent.from_name = "PEA Solar Hero";
                    mailContent.from_email = "peasolarhero@pea.co.th";
                    mailContent.to_name = toMainEmail.CONTRACT_NAME;
                    mailContent.to_email = toMainEmail.EMAIL;
                    mailContent.subject = "ติดตั้ง Solar Rooftop วันนี้! ประหยัดจริงตั้งแต่ติดตั้งและในระยะยาว";
                    mailContent.template_key = "40825e76ccdec39fd";
                    mailContent.content_html = conteneHtml;
                    mailContent.transactional_group_name = "EDM02";
                    mailContent.message_id = GenerateMessageId(customerProfileData.CUSTOMER_ID.ToString());

                    mailContent.cc = CCEmailList.Select(a => new CCProperty()
                    {
                        name = a.CONTRACT_NAME,
                        email = a.EMAIL
                    }).ToList();


                    if (CCEmailList.Count != 0)
                    {
                        var responseMail = sendEmailManagement.SendEmailTransactionalTemplateCC(mailContent);

                        if (responseMail.code == 202 && responseMail.data != null)
                        {
                            result = true;
                            foreach (var contratListData in CCEmailList)
                            {
                                edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, contratListData.ID);
                                if (edmH.Data == null)
                                {
                                    edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                    edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                    edmH.Data.CA_NO = customerProfileData.CA_NO;
                                    edmH.Data.EMD_TYPE = 0;
                                    edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                    edmH.Data.SEND_DATE = DateTime.Now;
                                    edmH.Data.LAST_EMAIL_STATUS = "W";
                                    edmH.Data.CONTRACT_ID = contratListData.ID;
                                    edmH.Data.EMAIL = contratListData.EMAIL;
                                    edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                    edmH.Create(edmH.Data);
                                }
                            }

                            edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, toMainEmail.ID);
                            if (edmH.Data == null)
                            {
                                edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                edmH.Data.CA_NO = customerProfileData.CA_NO;
                                edmH.Data.EMD_TYPE = 0;
                                edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                edmH.Data.SEND_DATE = DateTime.Now;
                                edmH.Data.LAST_EMAIL_STATUS = "W";
                                edmH.Data.CONTRACT_ID = toMainEmail.ID;
                                edmH.Data.EMAIL = toMainEmail.EMAIL;
                                edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                edmH.Create(edmH.Data);
                            }
                        }

                    }
                    else
                    {
                        var responseMail = sendEmailManagement.SendEmailTransactionalTemplateTo(mailContent);
                        if (responseMail.code == 202 && responseMail.data != null)
                        {
                            result = true;

                            edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, toMainEmail.ID);
                            if (edmH.Data == null)
                            {
                                edmH.Data = new RRPlatFormModel.Models.EDM_H();
                                edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                                edmH.Data.CA_NO = customerProfileData.CA_NO;
                                edmH.Data.EMD_TYPE = 0;
                                edmH.Data.PK_TAXI_ID = mailContent.message_id;
                                edmH.Data.SEND_DATE = DateTime.Now;
                                edmH.Data.LAST_EMAIL_STATUS = "W";
                                edmH.Data.CONTRACT_ID = toMainEmail.ID;
                                edmH.Data.EMAIL = toMainEmail.EMAIL;
                                edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                                edmH.Create(edmH.Data);
                            }
                        }
                    }

                }

                if (result)
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.SendEdm1Complete;
                else
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.Error;

                customerProfile.Edit("", customerProfileData);

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Status : " + customerProfileData.ACTIVITY_STATUS + " ---");

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");
            }

            log.Info("--- End Customer Count : " + customerProfile.Datas.Count + " ---");

            return result;

        }
        public bool AutoJobSendEmailEDM3()
        {
            bool result = false;



            return result;

        }
        public bool AutoJobSendEmailEDM4()
        {
            bool result = false;
            CustomerProfile customerProfile = new CustomerProfile();
            ContratList contratList = new ContratList();
            SendEmailManagement sendEmailManagement = new SendEmailManagement();
            EdmH edmH = new EdmH();

            customerProfile.Datas = customerProfile.GetListByIsActiveActivity(StatusYesNo.Yes.Value(), (int)ActivityStatus.WaitEdm4);

            log.Info("--- Start Customer Count : " + customerProfile.Datas.Count + " ---");

            foreach (var customerProfileData in customerProfile.Datas)
            {
                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");

                var MainEmailList = contratList.GetDatasByMainStatus(customerProfileData.CUSTOMER_ID.ToString(), StatusYesNo.Yes.Value());

                foreach (var toMainEmail in MainEmailList)
                {
                    result = false;

                    ContentEdm4Request contentRequest = new ContentEdm4Request();
                    contentRequest.CF_fullname = "คุณ " + toMainEmail.CONTRACT_NAME;
                    contentRequest.CF_company_name = customerProfileData.CUSTOMER_NAME;

                    string conteneHtml = JsonConvert.SerializeObject(contentRequest);

                    SendEmailTransactionalTemplateRequest mailContent = new SendEmailTransactionalTemplateRequest();
                    mailContent.priority = "0";
                    mailContent.from_name = "PEA Solar Hero";
                    mailContent.from_email = "peasolarhero@pea.co.th";
                    mailContent.to_name = toMainEmail.CONTRACT_NAME;
                    mailContent.to_email = toMainEmail.EMAIL;
                    mailContent.subject = "ขอบคุณที่เข้าร่วมโครงการ Solar Rooftop กับ PEA";
                    mailContent.template_key = "40825e76d2b7cb8fc";
                    mailContent.content_html = conteneHtml;
                    mailContent.transactional_group_name = "EDM04";
                    mailContent.message_id = GenerateMessageId(customerProfileData.CUSTOMER_ID.ToString());

                    var responseMail = sendEmailManagement.SendEmailTransactionalTemplateTo(mailContent);
                    if (responseMail.code == 202 && responseMail.data != null)
                    {
                        result = true;

                        edmH.Data = edmH.CheckDupicate(customerProfileData.CUSTOMER_ID, customerProfileData.CA_NO, mailContent.message_id, toMainEmail.ID);
                        if (edmH.Data == null)
                        {
                            edmH.Data = new RRPlatFormModel.Models.EDM_H();
                            edmH.Data.CUSTOMER_ID = customerProfileData.CUSTOMER_ID;
                            edmH.Data.CA_NO = customerProfileData.CA_NO;
                            edmH.Data.EMD_TYPE = 0;
                            edmH.Data.PK_TAXI_ID = mailContent.message_id;
                            edmH.Data.SEND_DATE = DateTime.Now;
                            edmH.Data.LAST_EMAIL_STATUS = "W";
                            edmH.Data.CONTRACT_ID = toMainEmail.ID;
                            edmH.Data.EMAIL = toMainEmail.EMAIL;
                            edmH.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                            edmH.Create(edmH.Data);
                        }
                    }


                }

                if (result)
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.WaitEdm4Complete;
                else
                    customerProfileData.ACTIVITY_STATUS = (int)ActivityStatus.Error;

                customerProfile.Edit("", customerProfileData);

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Status : " + customerProfileData.ACTIVITY_STATUS + " ---");

                log.Info("--- Start Customer No : " + customerProfileData.CUSTOMER_NO + " Name : " + customerProfileData.CUSTOMER_NAME + " ---");
            }

            log.Info("--- End Customer Count : " + customerProfile.Datas.Count + " ---");

            return result;

        }

        public bool AutoUpdateStatusCustomerListSendEdmComplete()
        {
            bool result = false;



            return result;

        }
   
        public bool AutoUpdateStatusCustomerListRegister()
        {
            bool result = false;



            return result;

        }

        public bool AutoJobSendEmailEDM5()
        {
            bool result = false;



            return result;

        }

        public bool AutoJobSendEmailEDM6()
        {
            bool result = false;



            return result;

        }

    }
    public class EmailContent
    {
        public string id { get; set; }
        public string message { get; set; }
    }
}
