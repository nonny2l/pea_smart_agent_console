﻿using RRApiFramework.Model;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrSendSmsH : AbRepository<TR_SMS_SENDING_H>
    {
        public override TR_SMS_SENDING_H GetData(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.ID == idInt && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public override List<TR_SMS_SENDING_H> GetDatas(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.CUSTOMER_ID == idInt && a.IS_ACTIVE == "Y").ToList();
        }

        public TR_SMS_SENDING_H CheckDupicate(int customerId ,string caNo, int contractId, int edmType)
        {
            return FindBy(a => a.CUSTOMER_ID == customerId 
                              && a.CA_NO == caNo 
                              && a.EMD_TYPE == edmType
                              && a.CONTRACT_ID == contractId
                              && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

       

        public TR_SMS_SENDING_H Save(int customerId, string caNo, int contractId, int edmType,
            string message, string sendingStatus,string messageId,string taskId)
        {
            this.Data = this.CheckDupicate(customerId, caNo, contractId, edmType);
            if (this.Data == null)
            {
                this.Data.CUSTOMER_ID = customerId;
                this.Data.CA_NO = caNo;
                this.Data.CONTRACT_ID = contractId;
                this.Data.EMD_TYPE = edmType;
                this.Data.SEND_STATUS = sendingStatus;
                this.Data.SEND_DATE = DateTime.Now;
                this.Data.MESSAGE_ID = messageId;
                this.Data.TASK_ID = taskId;
                this.Data.MESSAGE = message;
                this.Data.CREATED_BY = "admin";
                this.Data.CREATED_DATE = DateTime.Now;
                this.Create(this.Data);
            }
            else
            {
                this.Data.MESSAGE_ID = messageId;
                this.Data.TASK_ID = taskId;
                this.Data.EMD_TYPE = edmType;
                this.Data.SEND_STATUS = sendingStatus;
                this.Data.SEND_DATE = DateTime.Now;
                this.Data.MESSAGE = message;
                this.Data.UPDATED_BY = "admin";
                this.Data.UPDATED_DATE = DateTime.Now;
                this.Edit("", this.Data);
            }

            return this.Data;
        }
    }
}