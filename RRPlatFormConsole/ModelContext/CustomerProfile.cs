﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class CustomerProfile : AbRepository<CUSTOMER_PROFILE>
    {
        public override CUSTOMER_PROFILE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.CUSTOMER_ID == idInt).FirstOrDefault();
        }

        public  CUSTOMER_PROFILE GetDataByCustomerNo(string customerNo)
        {
            return this.FindBy(a => a.CUSTOMER_NO == customerNo && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<CUSTOMER_PROFILE> GetListByIsActiveActivity(string statusCode,int activityStatus)
        {
            return this.FindBy(a => a.IS_ACTIVE == statusCode && a.ACTIVITY_STATUS == activityStatus).ToList();
        }
        public List<CUSTOMER_PROFILE> GetListByUnGenarateBarcode()
        {
            return this.FindBy(a => a.IS_ACTIVE == "Y" && a.QR_CODE == null).ToList();
        }
        public CUSTOMER_PROFILE Save(CustomerProfileDto customerData)
        {
            CUSTOMER_PROFILE result = new CUSTOMER_PROFILE();
            this.Data = this.GetData(customerData.customerId);

            if (this.Data == null)
            {
                this.Data = this.BindDataDb(this.Data, customerData, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataDb(this.Data, customerData, ActionType.Add);
                this.Edit("",this.Data);
            }

            result = this.Data;

            return result;
        }

        public CUSTOMER_PROFILE BindDataDb(CUSTOMER_PROFILE data, CustomerProfileDto dataBind,ActionType actionType)
        {
            if (data != null)
                data = new CUSTOMER_PROFILE();

            data.CA_NO = dataBind.caNo;
            data.CUSTOMER_NO = dataBind.customerId;
            data.ADDRESS = dataBind.address;
            data.SUBDISTRICT = dataBind.subdistrict;
            data.DISTRICT = dataBind.district;
            data.PROVINCE = dataBind.province;
            data.REGION = dataBind.region;
            data.TEL01 = dataBind.tel1;
            data.TEL02 = dataBind.tel2;
            data.TEL03 = dataBind.tel3;
            data.AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE = dataBind.averageAmountOfElectricityUsage.ToString();
            data.SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE = dataBind.sumaryAmountOfElectricityUsage.ToString();
            data.SUB_BUSSINESS_TYPE = dataBind.subBusinessType.ToString();
            data.ACTIVITY_STATUS = dataBind.customerStatus.ToInt32();
            data.IS_ACTIVE = dataBind.isActive;

            if (actionType == ActionType.Add)
            {
                data.CREATED_BY = dataBind.createBy;
                data.CREATED_DATE = dataBind.createDate;
            }
            else
            {
                data.UPDATED_BY = dataBind.updateBy;
                data.UPDATED_DATE = dataBind.updateDate;
            }
            return data;
        }

        public CustomerProfileDto BindDataDto(CUSTOMER_PROFILE dataBind)
        {
            CustomerProfileDto data = new CustomerProfileDto();
            SourceData sourceData = new SourceData();

            data.customerId = dataBind.CUSTOMER_ID.ToString();
            data.caNo = dataBind.CA_NO;
            data.customerNo = dataBind.CUSTOMER_NO;
            data.address = dataBind.ADDRESS;
            data.subdistrict = dataBind.SUBDISTRICT;
            data.district = dataBind.DISTRICT;
            data.province = dataBind.PROVINCE;
            data.region = dataBind.REGION;
            data.tel1 = dataBind.TEL01;
            data.tel2 = dataBind.TEL02;
            data.tel3 = dataBind.TEL03;
            data.averageAmountOfElectricityUsage = dataBind.AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE.ToString();
            data.sumaryAmountOfElectricityUsage = dataBind.SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE.ToString();
            data.subBusinessType = dataBind.SUB_BUSSINESS_TYPE.ToString();
            data.dataSource = sourceData.GetSourceNameByCustomerId(dataBind.CUSTOMER_ID);
            data.customerStatus = dataBind.ACTIVITY_STATUS.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATED_BY;
            data.createDate = dataBind.CREATED_DATE;
            data.updateBy = dataBind.UPDATED_BY;
            data.updateDate = dataBind.UPDATED_DATE;
            return data;
        }

        public override List<CUSTOMER_PROFILE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }

    public class CustomerProfileDto : MasterDto
    {
        public string customerId { get; set; }
        public string campianCode { get; set; }
        public string caNo { get; set; }
        public string customerNo { get; set; }
        public string address { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string region { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }
        public string tel3 { get; set; }
        public string averageAmountOfElectricityUsage { get; set; }
        public string sumaryAmountOfElectricityUsage { get; set; }
        public string subBusinessType { get; set; }
        public List<string> dataSource { get; set; }
        public string customerStatus { get; set; }
    }
}