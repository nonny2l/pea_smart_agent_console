﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class ContratList : AbRepository<CONTRACT_LIST>
    {
        public override CONTRACT_LIST GetData(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.ID == idInt && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public override List<CONTRACT_LIST> GetDatas(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.CUSTOMER_ID == idInt && a.IS_ACTIVE == "Y").ToList();
        }

       


        public List<CONTRACT_LIST> GetDatasByMainStatus(string customerId,string mainFromStatus)
        {
            int idInt = customerId.ToInt32();
            return FindBy(a => a.CUSTOMER_ID == idInt && a.MAIN_FROM_STATUS == mainFromStatus
                                                      && a.EMAIL != null
                                                      && a.IS_ACTIVE == "Y").ToList();
        }
    }

}