﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUser : AbRepository<MT_USER>
    {
        public override MT_USER GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.USER_ID == idInt).FirstOrDefault();
        }

        public override List<MT_USER> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
}