﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public enum ActivityStatus
    {
        [DefaultValue(0)]
        Error = 0,

        [DefaultValue(1)]
        New = 1,

        [DefaultValue(2)]
        WaitEdm = 2,

        [DefaultValue(3)]
        SendEdm1Complete = 3,

        [DefaultValue(4)]
        WaitEdm4 = 4,

        [DefaultValue(5)]
        WaitEdm4Complete = 5,
    }
}