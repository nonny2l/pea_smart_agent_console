﻿using RRApiFramework.Model;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class EdmH : AbRepository<EDM_H>
    {
        public override EDM_H GetData(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.ID == idInt && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public override List<EDM_H> GetDatas(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.CUSTOMER_ID == idInt && a.IS_ACTIVE == "Y").ToList();
        }

        public EDM_H CheckDupicate(int customerId ,string caNo, string pkMailId,int contractId)
        {
            return FindBy(a => a.CUSTOMER_ID == customerId 
                              && a.CA_NO == caNo 
                              && a.PK_TAXI_ID == pkMailId
                              && a.CONTRACT_ID == contractId
                              && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
    }
}