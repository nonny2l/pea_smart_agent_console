﻿using log4net;
using RestSharp;
using RestSharp.Authenticators;
using RRPlatFormConsole.ExternalApiCaller;
using RRPlatFormConsole.SchedulerJob;
using RRPlatFormConsole.SendSMS;
using System;
using System.IO;

namespace RRPlatFormConsole
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            try
            {


                SchedulerJobAction schedulerJobAction = new SchedulerJobAction();
              //  SchedulerJobAction.SendSMSByRest("0923345352","Hello World ทดสอบภาษาไทย");

                SendSms.SendRequest("0923345352", "Hello World ทดสอบภาษาไทย");
                //if (args.Length > 0)
                //{
                //    log.Info("------ args : " + args[0] + "------");

                //    if (args[0] == "GEN_BARCODE")
                //    {
                //        log.Info("--- Start GEN_BARCODE ---");
                //        schedulerJobAction.AutoGenerateBarCode();
                //        log.Info("--- End GEN_BARCODE ---");
                //    }

                //    if (args[0] == "EDM1")
                //    {
                //        log.Info("--- Start EDM1 ---");
                //        schedulerJobAction.AutoJobSendEmailEDM1();
                //        log.Info("--- End EDM1 ---");
                //    }
                //    else if (args[0] == "EDM2")
                //    {
                //        log.Info("--- Start EDM2 ---");
                //        schedulerJobAction.AutoJobSendEmailEDM2();
                //        log.Info("--- End EDM2 ---");
                //    }
                //    else if (args[0] == "EDM3")
                //    {
                //        log.Info("--- Start EDM3 ---");
                //        schedulerJobAction.AutoJobSendEmailEDM3();
                //        log.Info("--- End EDM3 ---");
                //    }
                //    else if (args[0] == "EDM4")
                //    {
                //        log.Info("--- Start EDM4 ---");
                //        schedulerJobAction.AutoJobSendEmailEDM4();
                //        log.Info("--- End EDM4 ---");
                //    }
                //    else if (args[0] == "SMS_EDM5")
                //    {
                //        log.Info("--- Start SMS_EDM5 ---");
                //        schedulerJobAction.SendSmsEdm5();
                //        log.Info("--- End SMS_EDM5 ---");
                //    }

                //}
                //else
                //{
                //    log.Info("--- argument is null ---");
                //}

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }

    }
}
