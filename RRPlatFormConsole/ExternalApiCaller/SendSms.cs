﻿using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.Controllers.ExternalApiCaller;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormConsole.SendSMS;
using RRPlatFormConsole.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace RRPlatFormConsole.ExternalApiCaller
{
    public class SendSms
    {
        private const string _reservedCharacters = "!*'();:@&=+$,/?%#[]";

        public static string SendRequest(string mobileNo, string sms)
        {
            string bulkEndpoint = WebConfigurationManager.AppSettings["BulkEndpoint"].ToString();
            string bulkAccount = WebConfigurationManager.AppSettings["BulkAccount"].ToString();
            string bulkPassword = WebConfigurationManager.AppSettings["BulkPassword"].ToString();


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(bulkEndpoint);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            string parameters = $"ACCOUNT={bulkAccount}&PASSWORD={bulkPassword}&MOBILE={mobileNo}&MESSAGE={SendSMSRest.UrlEncode(sms)}&LANGUAGE={"T"}";
            try
            {

                Encoding thaiEnc = Encoding.GetEncoding("iso-8859-11");
                byte[] requestBytes = thaiEnc.GetBytes(parameters);

                httpWebRequest.ContentLength = requestBytes.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {

                    requestStream.Write(requestBytes, 0, requestBytes.Length);
                    requestStream.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {

                    var response = streamReader.ReadToEnd();
                    return response;
                }
            }
            catch (WebException ex)
            {
                var response = ex.ToString();
                return response;

            }


        }
        public static string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (_reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int)@char);
            }
            return sb.ToString();
        }
        public static ContentSmsResponse BindSmsResponse(string contentString)
        {
            ContentSmsResponse contentSmsResponse = new ContentSmsResponse();
            if (!contentString.IsNullOrEmptyWhiteSpace())
            {
                contentSmsResponse.STATUS = MessageManagement.FindStringFromIndex(contentString, "STATUS=", 3);
                contentSmsResponse.MESSAGE_ID = MessageManagement.FindStringFromIndex(contentString, "MESSAGE_ID=", 9);
                contentSmsResponse.TASK_ID = MessageManagement.FindStringFromIndex(contentString, "TASK_ID=", 7);
                contentSmsResponse.END = MessageManagement.FindStringFromIndex(contentString, "END=", 2);
            }

            return contentSmsResponse;

        }

    }
}
