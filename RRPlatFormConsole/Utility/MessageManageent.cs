﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormConsole.Utility
{
    public class MessageManagement
    {
        public static string FindStringFromIndex(string contentString, string stringFilter, int lengthIndex)
        {
            string result = string.Empty;

            int firstIndexInt = contentString.IndexOf(stringFilter) + stringFilter.Length;
            if (firstIndexInt != 0)
            {
                string stringFilterBr = contentString.Substring(firstIndexInt, lengthIndex);
                result = stringFilterBr.Replace("\r\n", "");
                result = result.Replace("\nM", "");
            }
            return result;
        }
    }
}
