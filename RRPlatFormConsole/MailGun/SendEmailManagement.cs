﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.Controllers.ExternalApiCaller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormConsole.MailGun
{
    public class SendEmailManagement
    {

        public ActionResultStatus SendEmailTransactional(SendEmailTransactionalRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TaxiLogin taxiLogin = new TaxiLogin();
            TaxiSendTransactional taxiSendTransactional = new TaxiSendTransactional();

            try
            {
                var loginData = taxiLogin.Login();
                if (loginData.code == 201 && loginData.data != null)
                {
                    SendTransactionalRequest requestEmail = new SendTransactionalRequest();
                    requestEmail.priority = "0";
                    requestEmail.from_name = request.from_name;
                    requestEmail.from_email = request.from_email;
                    requestEmail.to_name = request.to_name;
                    requestEmail.to_email = request.to_email;
                    requestEmail.subject = request.subject;
                    requestEmail.content_html = request.content_html;
                    requestEmail.content_plain = request.content_plain;
                    requestEmail.transactional_group_name = request.transactional_group_name;
                    requestEmail.message_id = request.message_id;
                    requestEmail.report_type = "Full";
                    requestEmail.session_id = loginData.data.session_id;
                    var taxiSendEmail = taxiSendTransactional.SendTransactional(requestEmail);

                    if (taxiSendEmail.code == 202 && taxiSendEmail.data != null)
                    {
                        mReturn.data = taxiSendEmail;
                    }
                    else
                    {
                        mReturn.success = loginData.status == "error" ? false : true;
                        mReturn.message = loginData.err_msg;
                        mReturn.code = loginData.code;
                    }
                }
                else
                {
                    mReturn.success = loginData.status == "error" ? false : true;
                    mReturn.message = loginData.err_msg;
                    mReturn.code = loginData.code;
                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public TaxiCCResponse SendEmailTransactionalTemplateCC(SendEmailTransactionalTemplateRequest request)
        {
            TaxiCCResponse mReturn = new TaxiCCResponse();
            TaxiLogin taxiLogin = new TaxiLogin();
            TaxiSendTransactionalTemplateCC taxiSendTransactionalMail = new TaxiSendTransactionalTemplateCC();
            try
            {
                var loginData = taxiLogin.Login();
                if (loginData.code == 201 && loginData.data != null)
                {
                    SendTransactionalTemplateRequest requestEmail = new SendTransactionalTemplateRequest();
                    requestEmail.priority = "0";
                    requestEmail.from_name = request.from_name;
                    requestEmail.from_email = request.from_email;
                    requestEmail.to_name = request.to_name;
                    requestEmail.to_email = request.to_email;
                    requestEmail.subject = request.subject;
                    requestEmail.content_html = request.content_html;
                    requestEmail.template_key = request.template_key;
                    requestEmail.transactional_group_name = request.transactional_group_name;
                    requestEmail.message_id = request.message_id;

                    requestEmail.report_type = "Full";
                    requestEmail.session_id = loginData.data.session_id;
                    requestEmail.cc = request.cc;
                    //string dataTest = JsonConvert.SerializeObject(requestEmail);

                    mReturn = taxiSendTransactionalMail.SendTransactionalTemplateCC(requestEmail); 
                }


            }
            catch (Exception ex)
            {
                mReturn.status = "False";
                mReturn.err_msg = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public TaxiResponse SendEmailTransactionalTemplateTo(SendEmailTransactionalTemplateRequest request)
        {
            TaxiResponse mReturn = new TaxiResponse();
            TaxiLogin taxiLogin = new TaxiLogin();
            TaxiSendTransactionalTemplateTo taxiSendTransactionalMail = new TaxiSendTransactionalTemplateTo();
            try
            {
                var loginData = taxiLogin.Login();
                if (loginData.code == 201 && loginData.data != null)
                {
                    SendTransactionalTemplateRequest requestEmail = new SendTransactionalTemplateRequest();
                    requestEmail.priority = "0";
                    requestEmail.from_name = request.from_name;
                    requestEmail.from_email = request.from_email;
                    requestEmail.to_name = request.to_name;
                    requestEmail.to_email = request.to_email;
                    requestEmail.subject = request.subject;
                    requestEmail.content_html = request.content_html;
                    requestEmail.template_key = request.template_key;
                    requestEmail.transactional_group_name = request.transactional_group_name;
                    requestEmail.message_id = request.message_id;
                    requestEmail.report_type = "Full";
                    requestEmail.session_id = loginData.data.session_id;
                    //string dataTest = JsonConvert.SerializeObject(requestEmail);
                    mReturn = taxiSendTransactionalMail.SendTransactionalTemplateTo(requestEmail);
                }


            }
            catch (Exception ex)
            {
                mReturn.status = "False";
                mReturn.err_msg = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


    }
    public class ContentRequest
    {
        public string CF_url { get; set; }
        public string CF_fullname { get; set; }
        public string CF_qr_code { get; set; }
        public string CF_send_date { get; set; }
    }
    public class ContentEdm4Request
    {
        public string CF_fullname { get; set; }
        public string CF_company_name { get; set; }
    }

    public class SendEmailTransactionalRequest
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string content_html { get; set; }
        public string content_plain { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }
    }

    public class SendEmailTransactionalTemplateRequest
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string template_key { get; set; }
        public string content_html { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }

        public string bcc { get; set; }
        public string attachment { get; set; }
        public List<CCProperty> cc { get; set; }

    }
    public class CCProperty
    {
        public string name { get; set; }
        public string email { get; set; }
    }
}
